<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@include file="../includes/nav.jsp" %>
<main>
    <div class="container-fluid px-4">
        <h1 class="mt-4">Category</h1>
        <div class="row">
            <table border="1">
            <thead>
                <tr>
                    <th>Id</th>
                    <th>Tên danh mục</th>
                    <th>Trạng thái</th>
                    <th>Option</th>
                </tr>
            </thead>
            <tbody>
                <c:forEach items="${data}" var="c">
                    <tr>
                        <th>${c.id}</th>
                        <th>${c.name}</th>
                        <th>${c.status}</th>
                    </tr>
                </c:forEach>
            </tbody>
        </table>
        </div>
    </div>
</main>
<%@include file="../includes/footer.jsp" %>
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import javax.validation.Valid;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 *
 * @author DUY
 */
@Controller
public class HomeController {
    @RequestMapping(value = "/Trang-chu", method = RequestMethod.GET)
    public String index(Model m){
        return "client/index";
    }
    
    @RequestMapping(value = "/about", method = RequestMethod.GET)
    public String about(Model m){
        return "client/about";
    }
    
    @RequestMapping(value = "/shop", method = RequestMethod.GET)
    public String shop(Model m){
        return "client/shop";
    }
    @RequestMapping(value = "/contact", method = RequestMethod.GET)
    public String contact(Model m){
        return "client/contact";
    }
   
}

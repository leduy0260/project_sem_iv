/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import dao.CategoryDao;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 *
 * @author DUY
 */
@Controller
public class AdminController {

    CategoryDao dao;

    public AdminController() {
        dao = new CategoryDao();
    }

    @RequestMapping(value = "/Login", method = RequestMethod.GET)
    public String login(Model m) {
        return "admin/login";
    }

    @RequestMapping(value = "/Admin", method = RequestMethod.GET)
    public String admin(Model m) {
        return "admin/index";
    }

    @RequestMapping(value = "login", method = RequestMethod.GET)
    public String save(String email, String password, Model m) {
        if (email.equals("leduy0260@gmail.com") && password.equals("12345")) {
            return "redirect:Admin.htm";
        }
        return "admin/login";
    }

    @RequestMapping(value = "category", method = RequestMethod.GET)
    public String category(Model m) {
        m.addAttribute("data", dao.paging());
        return "admin/category";
    }
}

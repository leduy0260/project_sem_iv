/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import java.util.List;

/**
 *
 * @author DUY
 */
public interface IDao<Entity> {
    public List<Entity> paging();
    public void insert(Entity e);
    public void update(Entity e);
    public void delete(Entity e);
    public Entity searchById(int id);
    public List<Entity> search(String _param);
}

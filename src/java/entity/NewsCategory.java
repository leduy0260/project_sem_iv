/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author DUY
 */
@Entity
@Table(name = "newsCategory")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "NewsCategory.findAll", query = "SELECT n FROM NewsCategory n"),
    @NamedQuery(name = "NewsCategory.findById", query = "SELECT n FROM NewsCategory n WHERE n.id = :id"),
    @NamedQuery(name = "NewsCategory.findByName", query = "SELECT n FROM NewsCategory n WHERE n.name = :name"),
    @NamedQuery(name = "NewsCategory.findByStatus", query = "SELECT n FROM NewsCategory n WHERE n.status = :status")})
public class NewsCategory implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @NotNull
    @Column(name = "id")
    private Integer id;
    @Size(max = 20)
    @Column(name = "name")
    private String name;
    @Column(name = "status")
    private Integer status;
    @OneToMany(mappedBy = "newsCategoryId")
    private Collection<News> newsCollection;

    public NewsCategory() {
    }

    public NewsCategory(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    @XmlTransient
    public Collection<News> getNewsCollection() {
        return newsCollection;
    }

    public void setNewsCollection(Collection<News> newsCollection) {
        this.newsCollection = newsCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof NewsCategory)) {
            return false;
        }
        NewsCategory other = (NewsCategory) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.NewsCategory[ id=" + id + " ]";
    }
    
}
